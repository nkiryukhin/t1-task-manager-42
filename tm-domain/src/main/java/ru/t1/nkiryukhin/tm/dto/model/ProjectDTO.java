package ru.t1.nkiryukhin.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.model.IWBS;
import ru.t1.nkiryukhin.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public final class ProjectDTO extends AbstractUserOwnedModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

    @Column
    @NotNull
    private Date created = new Date();

    @Column
    @Nullable
    private String description = "";

    @Column
    @NotNull
    private String name = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;


    public ProjectDTO(@NotNull String name) {
        this.name = name;
    }

    public ProjectDTO(@NotNull String name, @Nullable String description) {
        this.name = name;
        this.description = description;
    }

    public ProjectDTO(@NotNull String name, @NotNull String description, @NotNull Status status) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description + " : " + status.getDisplayName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectDTO project = (ProjectDTO) o;
        return Objects.equals(getId(), project.getId())
                && Objects.equals(created, project.created)
                && Objects.equals(description, project.description)
                && name.equals(project.name)
                && status == project.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

}