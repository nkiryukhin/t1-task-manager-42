package ru.t1.nkiryukhin.tm.data;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.comparator.NameComparator;
import ru.t1.nkiryukhin.tm.dto.model.ProjectDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static ru.t1.nkiryukhin.tm.data.UserTestData.ADMIN_USER;

@UtilityClass
public final class ProjectTestData {

    @NotNull
    public final static ProjectDTO USUAL_PROJECT1 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO USUAL_PROJECT2 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO ADMIN_PROJECT1 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO ADMIN_PROJECT2 = new ProjectDTO();

    @Nullable
    public final static ProjectDTO NULL_PROJECT = null;

    @NotNull
    public final static String NON_EXISTING_PROJECT_ID = UUID.randomUUID().toString();

    @NotNull
    public final static List<ProjectDTO> USUAL_PROJECT_LIST = Arrays.asList(USUAL_PROJECT1, USUAL_PROJECT2);

    @NotNull
    public final static List<ProjectDTO> ADMIN_PROJECT_LIST = Arrays.asList(ADMIN_PROJECT1, ADMIN_PROJECT2);

    @NotNull
    public final static List<ProjectDTO> PROJECT_LIST = new ArrayList<>();

    @NotNull
    public final static List<ProjectDTO> SORTED_PROJECT_LIST = new ArrayList<>();

    static {
        USUAL_PROJECT_LIST.forEach(project -> project.setUserId(UserTestData.USUAL_USER.getId()));
        USUAL_PROJECT1.setName("Usual Project 1");
        USUAL_PROJECT2.setName("Usual Project 2");
        USUAL_PROJECT1.setDescription("Usual Project 1 Desc");
        USUAL_PROJECT2.setDescription("Usual Project 2 Desc");
        ADMIN_PROJECT_LIST.forEach(project -> project.setUserId(ADMIN_USER.getId()));
        ADMIN_PROJECT1.setName("Admin Project 1");
        ADMIN_PROJECT2.setName("Admin Project 2");
        ADMIN_PROJECT1.setDescription("Admin Project 1 Desc");
        ADMIN_PROJECT2.setDescription("Admin Project 2 Desc");
        ADMIN_PROJECT1.setUserId(ADMIN_USER.getId());
        ADMIN_PROJECT2.setUserId(ADMIN_USER.getId());
        PROJECT_LIST.addAll(USUAL_PROJECT_LIST);
        PROJECT_LIST.addAll(ADMIN_PROJECT_LIST);
        SORTED_PROJECT_LIST.addAll(PROJECT_LIST);
        SORTED_PROJECT_LIST.sort(NameComparator.INSTANCE);
        USUAL_PROJECT_LIST.sort(NameComparator.INSTANCE);
    }

}
