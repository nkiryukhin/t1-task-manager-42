package ru.t1.nkiryukhin.tm.data;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.model.SessionDTO;
import ru.t1.nkiryukhin.tm.enumerated.Role;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@UtilityClass
public final class SessionTestData {

    @NotNull
    public final static SessionDTO USUAL_SESSION1 = new SessionDTO();

    @NotNull
    public final static SessionDTO USUAL_SESSION2 = new SessionDTO();

    @NotNull
    public final static SessionDTO ADMIN_SESSION1 = new SessionDTO();

    @NotNull
    public final static SessionDTO ADMIN_SESSION2 = new SessionDTO();

    @Nullable
    public final static SessionDTO NULL_SESSION = null;

    @NotNull
    public final static String NON_EXISTING_SESSION_ID = UUID.randomUUID().toString();

    @NotNull
    public final static List<SessionDTO> USER_SESSION_LIST = Arrays.asList(USUAL_SESSION1, USUAL_SESSION2);

    @NotNull
    public final static List<SessionDTO> ADMIN_SESSION_LIST = Arrays.asList(ADMIN_SESSION1, ADMIN_SESSION2);

    @NotNull
    public final static List<SessionDTO> SESSION_LIST = new ArrayList<>();

    static {
        USER_SESSION_LIST.forEach(session -> session.setUserId(UserTestData.USUAL_USER.getId()));
        USER_SESSION_LIST.forEach(session -> session.setRole(Role.USUAL));
        ADMIN_SESSION_LIST.forEach(session -> session.setUserId(UserTestData.ADMIN_USER.getId()));
        ADMIN_SESSION_LIST.forEach(session -> session.setRole(Role.ADMIN));
        SESSION_LIST.addAll(USER_SESSION_LIST);
        SESSION_LIST.addAll(ADMIN_SESSION_LIST);
    }

}