package ru.t1.nkiryukhin.tm.data;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.dto.model.UserDTO;
import ru.t1.nkiryukhin.tm.service.PropertyService;
import ru.t1.nkiryukhin.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@UtilityClass
public final class UserTestData {

    @NotNull
    public final static UserDTO USUAL_USER = new UserDTO();

    @NotNull
    public final static String USUAL_USER_LOGIN = "USUAL_USER_LOGIN";

    @NotNull
    public final static String USUAL_USER_PASSWORD = "USUAL_USER_PASSWORD";

    @NotNull
    public final static String USUAL_USER_EMAIL = "USUAL_USER_EMAIL";

    @NotNull
    public final static UserDTO ADMIN_USER = new UserDTO();

    @NotNull
    public final static String ADMIN_USER_LOGIN = "ADMIN_USER_LOGIN";

    @NotNull
    public final static String ADMIN_USER_PASSWORD = "ADMIN_USER_PASSWORD";

    @NotNull
    public final static String ADMIN_USER_EMAIL = "ADMIN_USER_EMAIL";

    @NotNull
    public static final UserDTO ADMIN_USER_2 = new UserDTO();

    @NotNull
    public final static String ADMIN_USER_2_LOGIN = "ADMIN_USER_2_LOGIN";

    @NotNull
    public final static String ADMIN_USER_2_PASSWORD = "ADMIN_USER_2_PASSWORD";

    @NotNull
    public final static String ADMIN_USER_2_EMAIL = "ADMIN_USER_2_EMAIL";

    @Nullable
    public final static UserDTO NULL_USER = null;

    @NotNull
    public final static List<UserDTO> USER_LIST = new ArrayList<>();

    @NotNull
    public final static List<UserDTO> ADMIN_USER_LIST = new ArrayList<>();

    @NotNull
    public final static String NON_EXISTING_USER_ID = UUID.randomUUID().toString();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    static {
        USUAL_USER.setLogin(USUAL_USER_LOGIN);
        USUAL_USER.setPasswordHash(HashUtil.salt(propertyService, USUAL_USER_PASSWORD));
        USUAL_USER.setEmail(USUAL_USER_EMAIL);
        ADMIN_USER.setLogin(ADMIN_USER_LOGIN);
        ADMIN_USER.setPasswordHash(HashUtil.salt(propertyService, ADMIN_USER_PASSWORD));
        ADMIN_USER.setEmail(ADMIN_USER_EMAIL);
        ADMIN_USER_2.setLogin(ADMIN_USER_2_LOGIN);
        ADMIN_USER_2.setPasswordHash(HashUtil.salt(propertyService, ADMIN_USER_2_PASSWORD));
        ADMIN_USER_2.setEmail(ADMIN_USER_2_EMAIL);
        ADMIN_USER_LIST.add(ADMIN_USER);
        ADMIN_USER_LIST.add(ADMIN_USER_2);
        USER_LIST.add(USUAL_USER);
        USER_LIST.addAll(ADMIN_USER_LIST);
    }

}