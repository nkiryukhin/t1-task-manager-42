package ru.t1.nkiryukhin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.service.*;
import ru.t1.nkiryukhin.tm.dto.model.TaskDTO;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.nkiryukhin.tm.exception.entity.TaskNotFoundException;
import ru.t1.nkiryukhin.tm.exception.field.ProjectIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.TaskIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;

import static ru.t1.nkiryukhin.tm.data.ProjectTestData.*;
import static ru.t1.nkiryukhin.tm.data.TaskTestData.*;
import static ru.t1.nkiryukhin.tm.data.UserTestData.USUAL_USER;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull static final IPropertyService propertyService = new PropertyService();

    @NotNull static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull static final ITaskService taskService = new TaskService(connectionService);

    @NotNull static final IProjectService projectService = new ProjectService(connectionService);

    @NotNull static final IProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService);

    @NotNull static final ISessionService sessionService = new SessionService(connectionService);

    @NotNull static final IUserService userService = new UserService(connectionService, propertyService, taskService, projectService, sessionService);

    @BeforeClass
    public static void setUp() {
        userService.add(USUAL_USER);
    }

    @Before
    public void before() {
        projectService.add(USUAL_PROJECT1);
        projectService.add(USUAL_PROJECT2);
        taskService.add(USUAL_TASK1);
        taskService.add(USUAL_TASK2);
    }

    @After
    public void after() throws AbstractException {
        taskService.remove(TASK_LIST);
        projectService.remove(PROJECT_LIST);
    }

    @AfterClass
    public static void tearDown() throws UserIdEmptyException {
        userService.removeOne(USUAL_USER);
    }

    @Test
    public void bindTaskToProject() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectTaskService.bindTaskToProject(null, USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectTaskService.bindTaskToProject("", USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            projectTaskService.bindTaskToProject(USUAL_USER.getId(), null, USUAL_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            projectTaskService.bindTaskToProject(USUAL_USER.getId(), "", USUAL_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            projectTaskService.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            projectTaskService.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            projectTaskService.bindTaskToProject(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID, USUAL_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            projectTaskService.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        projectTaskService.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT2.getId(), USUAL_TASK1.getId());
        TaskDTO task = taskService.findOneById(USUAL_USER.getId(), USUAL_TASK1.getId());
        Assert.assertEquals(USUAL_PROJECT2.getId(), task.getProjectId());
        projectTaskService.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
    }

    @Test
    public void removeProjectById() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectTaskService.removeProjectById(null, USUAL_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectTaskService.removeProjectById("", USUAL_PROJECT1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            projectTaskService.removeProjectById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            projectTaskService.removeProjectById(USUAL_USER.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            projectTaskService.removeProjectById(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID);
        });
        projectTaskService.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        projectTaskService.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK2.getId());
        projectTaskService.removeProjectById(USUAL_USER.getId(), USUAL_PROJECT1.getId());
        Assert.assertNull(projectService.findOneById(USUAL_USER.getId(), USUAL_PROJECT1.getId()));
        Assert.assertNull(taskService.findOneById(USUAL_USER.getId(), USUAL_TASK1.getId()));
        Assert.assertNull(taskService.findOneById(USUAL_USER.getId(), USUAL_TASK2.getId()));
    }

    @Test
    public void unbindTaskFromProject() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectTaskService.unbindTaskFromProject(null, USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectTaskService.unbindTaskFromProject("", USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            projectTaskService.unbindTaskFromProject(USUAL_USER.getId(), null, USUAL_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            projectTaskService.unbindTaskFromProject(USUAL_USER.getId(), "", USUAL_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            projectTaskService.unbindTaskFromProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            projectTaskService.unbindTaskFromProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            projectTaskService.unbindTaskFromProject(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID, USUAL_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            projectTaskService.unbindTaskFromProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        projectTaskService.unbindTaskFromProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        TaskDTO task = taskService.findOneById(USUAL_USER.getId(), USUAL_TASK1.getId());
        Assert.assertNull(task.getProjectId());
        projectTaskService.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
    }

}

