package ru.t1.nkiryukhin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.service.*;
import ru.t1.nkiryukhin.tm.comparator.NameComparator;
import ru.t1.nkiryukhin.tm.dto.model.ProjectDTO;
import ru.t1.nkiryukhin.tm.enumerated.Sort;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.nkiryukhin.tm.exception.field.*;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static ru.t1.nkiryukhin.tm.data.ProjectTestData.*;
import static ru.t1.nkiryukhin.tm.data.UserTestData.ADMIN_USER;
import static ru.t1.nkiryukhin.tm.data.UserTestData.USUAL_USER;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull static final IPropertyService propertyService = new PropertyService();

    @NotNull static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull static final IProjectService projectService = new ProjectService(connectionService);

    @NotNull static final ITaskService taskService = new TaskService(connectionService);

    @NotNull static final ISessionService sessionService = new SessionService(connectionService);

    @NotNull static final IUserService userService = new UserService(connectionService, propertyService, taskService, projectService, sessionService);

    @BeforeClass
    public static void setUp() {
        userService.add(USUAL_USER);
        userService.add(ADMIN_USER);
    }

    @Before
    public void before() {
        projectService.add(USUAL_PROJECT1);
        projectService.add(USUAL_PROJECT2);
    }

    @After
    public void after() throws AbstractException {
        projectService.remove(PROJECT_LIST);
    }

    @AfterClass
    public static void tearDown() throws UserIdEmptyException {
        userService.removeOne(ADMIN_USER);
        userService.removeOne(USUAL_USER);
    }

    @Test
    public void add() throws AbstractFieldException {
        Assert.assertNull(projectService.add(NULL_PROJECT));
        Assert.assertNotNull(projectService.add(ADMIN_PROJECT1));
        @Nullable final ProjectDTO project = projectService.findOneById(ADMIN_USER.getId(), ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1, project);
    }

    @Test
    public void changeProjectStatusById() throws AbstractException {
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectService.changeProjectStatusById(null, USUAL_PROJECT1.getId(), status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectService.changeProjectStatusById("", USUAL_PROJECT1.getId(), status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            projectService.changeProjectStatusById(USUAL_USER.getId(), null, status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            projectService.changeProjectStatusById(USUAL_USER.getId(), "", status);
        });
        Assert.assertThrows(StatusEmptyException.class, () -> {
            projectService.changeProjectStatusById(USUAL_USER.getId(), USUAL_PROJECT1.getId(), null);
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            projectService.changeProjectStatusById(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID, status);
        });
        projectService.changeProjectStatusById(USUAL_USER.getId(), USUAL_PROJECT1.getId(), status);
        ProjectDTO updatedProject = projectService.findOneById(USUAL_USER.getId(), USUAL_PROJECT1.getId());
        Assert.assertEquals(status, updatedProject.getStatus());
    }

    @Test
    public void changeProjectStatusByIndex() throws AbstractException {
        @NotNull final Status status = Status.COMPLETED;
        ProjectDTO project = projectService.findOneById(USUAL_USER.getId(), USUAL_PROJECT1.getId());
        Assert.assertNotNull(project);
        final int index = projectService.findAll(USUAL_USER.getId()).indexOf(project);

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectService.changeProjectStatusByIndex(null, index, status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectService.changeProjectStatusByIndex("", index, status);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            projectService.changeProjectStatusByIndex(USUAL_USER.getId(), null, status);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            projectService.changeProjectStatusByIndex(USUAL_USER.getId(), -1, status);
        });
        projectService.changeProjectStatusByIndex(USUAL_USER.getId(), index, status);
        project = projectService.findOneById(USUAL_USER.getId(), USUAL_PROJECT1.getId());
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void clearByUserId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectService.clear(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectService.clear("");
        });
        projectService.clear(USUAL_USER.getId());
        Assert.assertTrue(projectService.findAll(USUAL_USER.getId()).isEmpty());
    }

    @Test
    public void create() throws AbstractFieldException {
        Assert.assertThrows(NameEmptyException.class, () -> {
            projectService.create(ADMIN_USER.getId(), "", null);
        });
        @NotNull final ProjectDTO project = projectService.create(ADMIN_USER.getId(), ADMIN_PROJECT1.getName(), "desc");
        Assert.assertEquals(project, projectService.findOneById(ADMIN_USER.getId(), project.getId()));
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_USER.getId(), project.getUserId());
        PROJECT_LIST.add(project); //добавить в список новый проект, чтобы его удалить в after()
    }

    @Test
    public void existsById() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectService.existsById(null, NON_EXISTING_PROJECT_ID);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectService.existsById("", NON_EXISTING_PROJECT_ID);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            projectService.existsById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            projectService.existsById(USUAL_USER.getId(), "");
        });
        Assert.assertFalse(projectService.existsById(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(projectService.existsById(USUAL_USER.getId(), USUAL_PROJECT1.getId()));
    }

    @Test
    public void findAll() throws UserIdEmptyException {
        List<ProjectDTO> projects = projectService.findAll(USUAL_USER.getId());
        projects.sort(NameComparator.INSTANCE);
        Assert.assertEquals(USUAL_PROJECT_LIST, projects);
    }

    @Test
    public void findAllByUserId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectService.findAll("");
        });
        List<ProjectDTO> foundProjects = projectService.findAll(USUAL_USER.getId());
        foundProjects.sort(NameComparator.INSTANCE);
        Assert.assertEquals(USUAL_PROJECT_LIST, foundProjects);
    }

    @Test
    public void findAllComparatorByUserId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Comparator comparatorInner = null;
            projectService.findAll("", comparatorInner);
        });
        Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(
                USUAL_PROJECT_LIST.stream().sorted(comparator).collect(Collectors.toList()),
                projectService.findAll(USUAL_USER.getId(), comparator)
        );
    }

    @Test
    public void findAllSortByUserId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Sort sortInner = null;
            projectService.findAll("", sortInner);
        });
        Sort sort = Sort.BY_NAME;
        Assert.assertEquals(
                USUAL_PROJECT_LIST.stream().sorted(sort.getComparator()).collect(Collectors.toList()),
                projectService.findAll(USUAL_USER.getId(), sort.getComparator())
        );
    }

    @Test
    public void findOneById() throws AbstractFieldException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            projectService.findOneById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            projectService.findOneById(USUAL_USER.getId(), "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectService.existsById(null, USUAL_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectService.existsById("", USUAL_PROJECT1.getId());
        });
        Assert.assertNull(projectService.findOneById(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = projectService.findOneById(USUAL_USER.getId(), USUAL_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USUAL_PROJECT1, project);
    }

    @Test
    public void findOneByIndex() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectService.findOneByIndex(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectService.findOneByIndex("", null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            projectService.findOneByIndex(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            projectService.findOneByIndex(USUAL_USER.getId(), -1);
        });
        final int index = projectService.findAll(USUAL_USER.getId()).indexOf(USUAL_PROJECT1);
        @Nullable final ProjectDTO project = projectService.findOneByIndex(USUAL_USER.getId(), index);
        Assert.assertNotNull(project);
        Assert.assertEquals(USUAL_PROJECT1, project);
    }

    @Test
    public void getSize() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectService.getSize(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectService.getSize("");
        });
        Assert.assertEquals(0, projectService.getSize(ADMIN_USER.getId()));
        projectService.add(ADMIN_PROJECT1);
        Assert.assertEquals(1, projectService.getSize(ADMIN_USER.getId()));
    }

    @Test
    public void removeOne() throws AbstractException {
        @Nullable final ProjectDTO createdProject = projectService.add(ADMIN_PROJECT1);
        Assert.assertNotNull(projectService.findOneById(ADMIN_USER.getId(), ADMIN_PROJECT1.getId()));
        projectService.removeOne(createdProject);
        Assert.assertNull(projectService.findOneById(ADMIN_USER.getId(), ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeById() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectService.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectService.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            projectService.removeById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            projectService.removeById(USUAL_USER.getId(), "");
        });
        @Nullable final ProjectDTO createdProject = projectService.add(ADMIN_PROJECT1);
        projectService.removeById(ADMIN_USER.getId(), createdProject.getId());
        Assert.assertNull(projectService.findOneById(ADMIN_USER.getId(), ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeByIndex() throws AbstractException {
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            projectService.removeByIndex(ADMIN_USER.getId(), null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            projectService.removeByIndex(ADMIN_USER.getId(),-1);
        });
        @Nullable final ProjectDTO createdProject = projectService.add(ADMIN_PROJECT1);
        final int index = projectService.findAll(ADMIN_USER.getId()).indexOf(createdProject);
        projectService.removeByIndex(ADMIN_USER.getId(), index);
        Assert.assertNull(projectService.findOneById(ADMIN_USER.getId(), ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeAll() throws AccessDeniedException, UserIdEmptyException {
        int countProjects = projectService.getSize(USUAL_USER.getId());
        projectService.removeAll(USUAL_USER.getId());
        Assert.assertEquals(2, countProjects - projectService.getSize(USUAL_USER.getId()));
    }

    @Test
    public void updateById() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectService.updateById(null, USUAL_PROJECT1.getId(), USUAL_PROJECT1.getName(), USUAL_PROJECT1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectService.updateById("", USUAL_PROJECT1.getId(), USUAL_PROJECT1.getName(), USUAL_PROJECT1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            projectService.updateById(USUAL_USER.getId(), null, USUAL_PROJECT1.getName(), USUAL_PROJECT1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            projectService.updateById(USUAL_USER.getId(), "", USUAL_PROJECT1.getName(), USUAL_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            projectService.updateById(USUAL_USER.getId(), USUAL_PROJECT1.getId(), null, USUAL_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            projectService.updateById(USUAL_USER.getId(), USUAL_PROJECT1.getId(), "", USUAL_PROJECT1.getDescription());
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            projectService.updateById(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID, USUAL_PROJECT1.getName(), USUAL_PROJECT1.getDescription());
        });
        @NotNull final String name = USUAL_PROJECT1.getName() + NON_EXISTING_PROJECT_ID;
        @NotNull final String description = USUAL_PROJECT1.getDescription() + NON_EXISTING_PROJECT_ID;
        projectService.updateById(USUAL_USER.getId(), USUAL_PROJECT1.getId(), name, description);
        ProjectDTO updatedProject = projectService.findOneById(USUAL_USER.getId(), USUAL_PROJECT1.getId());
        Assert.assertEquals(name, updatedProject.getName());
        Assert.assertEquals(description, updatedProject.getDescription());
    }

}

