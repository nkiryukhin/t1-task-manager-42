package ru.t1.nkiryukhin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.model.UserDTO;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.EmailEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.LoginEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;

import java.util.List;

public interface IUserService {

    @Nullable
    UserDTO add(@Nullable final UserDTO user);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password) throws AbstractException;

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email) throws AbstractException;

    @NotNull
    UserDTO create(String login, String password, Role role) throws AbstractException;

    boolean existsById(@Nullable String id) throws AbstractFieldException;

    @Nullable
    List<UserDTO> findAll();

    @Nullable
    UserDTO findByLogin(@Nullable String login) throws LoginEmptyException;

    @Nullable
    UserDTO findByEmail(@Nullable String email) throws EmailEmptyException;

    @Nullable
    UserDTO findOneById(@Nullable String id) throws AbstractFieldException;

    int getSize();

    void remove(@Nullable List<UserDTO> users) throws UserIdEmptyException;

    void removeByLogin(@Nullable String login) throws AbstractException;

    void removeByEmail(@Nullable String email) throws AbstractException;

    void removeById(@Nullable String id) throws AbstractException;

    void removeOne(@Nullable UserDTO user) throws UserIdEmptyException;

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password) throws AbstractException;

    @NotNull
    UserDTO updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName,
            @Nullable String email
    ) throws AbstractException;

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login) throws AbstractException;

    void unlockUserByLogin(@Nullable String login) throws AbstractException;

}