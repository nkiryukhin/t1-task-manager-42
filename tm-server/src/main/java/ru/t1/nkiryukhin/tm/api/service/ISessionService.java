package ru.t1.nkiryukhin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.model.SessionDTO;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;

import java.util.List;

public interface ISessionService {

    SessionDTO add(@Nullable SessionDTO session);

    boolean existsById(@Nullable String id) throws AbstractFieldException;

    List<SessionDTO> findAll(@Nullable String userId) throws UserIdEmptyException;

    SessionDTO findOneById(@Nullable String id) throws AbstractFieldException;

    int getSize();

    void remove(List<SessionDTO> sessions);

    void removeById(@Nullable String id) throws AbstractFieldException;

    void removeOne(@Nullable SessionDTO session);

    void removeAllByUserId(@Nullable String userId) throws UserIdEmptyException;

}