package ru.t1.nkiryukhin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (id, created, name, description, status, user_id)" +
                                " VALUES (#{id}, #{created}, #{name}, #{description}, #{status}::status, #{userId})")
    void add(@NotNull ProjectDTO project);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<ProjectDTO> findAll(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY name")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<ProjectDTO> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<ProjectDTO> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY status")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<ProjectDTO> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @Nullable ProjectDTO findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @Nullable ProjectDTO findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(1) FROM tm_project WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    void removeById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    void remove(@NotNull ProjectDTO project);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void removeAll(@NotNull @Param("userId") String userId);

    @Update("UPDATE tm_project SET name = #{name}, description = #{description}, " +
            "status = #{status}::status WHERE id = #{id}")
    void update(@NotNull ProjectDTO project);

}