package ru.t1.nkiryukhin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.nkiryukhin.tm.api.endpoint.IUserEndpoint;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.dto.model.UserDTO;
import ru.t1.nkiryukhin.tm.dto.request.*;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.marker.IntegrationCategory;
import ru.t1.nkiryukhin.tm.service.PropertyService;

import java.util.Locale;

import static ru.t1.nkiryukhin.tm.data.UserTestData.*;

@Category(IntegrationCategory.class)
public final class UserEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void setUp() throws AbstractException {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(ADMIN_USER_LOGIN);
        loginRequest.setPassword(ADMIN_USER_PASSWORD);
        adminToken = authEndpoint.login(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USUAL_USER_LOGIN);
        request.setPassword(USUAL_USER_PASSWORD);
        request.setEmail(USUAL_USER_EMAIL);
        userEndpoint.registryUser(request);
    }

    @AfterClass
    public static void tearDown() throws AbstractException {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(USUAL_USER_LOGIN);
        userEndpoint.removeUser(request);
    }

    @Nullable
    private String getUserToken(final String login, final String password) throws AbstractException {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        return authEndpoint.login(request).getToken();
    }

    @Test
    public void changePasswordUser() throws AbstractException {
        @Nullable String token = getUserToken(USUAL_USER_LOGIN, USUAL_USER_PASSWORD);
        Assert.assertNotNull(token);
        @NotNull final UserChangePasswordRequest changePasswordRequest = new UserChangePasswordRequest(token);
        changePasswordRequest.setPassword(USUAL_USER_PASSWORD.toUpperCase(Locale.ROOT));
        Assert.assertNotNull(userEndpoint.changeUserPassword(changePasswordRequest));
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(token);
        authEndpoint.logout(logoutRequest);
        token = getUserToken(USUAL_USER_LOGIN, USUAL_USER_PASSWORD.toUpperCase(Locale.ROOT));
        Assert.assertNotNull(token);
        @NotNull final UserChangePasswordRequest reChangePasswordRequest = new UserChangePasswordRequest(token);
        reChangePasswordRequest.setPassword(USUAL_USER_PASSWORD);
        Assert.assertNotNull(userEndpoint.changeUserPassword(reChangePasswordRequest));
    }

    @Test
    public void lockUser() throws AbstractException {
        @NotNull final UserLockRequest request = new UserLockRequest(adminToken);
        request.setLogin(USUAL_USER_LOGIN);
        Assert.assertNotNull(userEndpoint.lockUser(request));
        Assert.assertThrows(Exception.class, () -> getUserToken(USUAL_USER_LOGIN, USUAL_USER_PASSWORD));
        unlockUser();
    }

    @Test
    public void registryUser() throws AbstractException {
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken);
        removeRequest.setLogin(USUAL_USER_LOGIN);
        userEndpoint.removeUser(removeRequest);
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USUAL_USER_LOGIN);
        request.setPassword(USUAL_USER_PASSWORD);
        request.setEmail(USUAL_USER_EMAIL);
        userEndpoint.registryUser(request);
        Assert.assertNotNull(getUserToken(USUAL_USER_LOGIN, USUAL_USER_PASSWORD));
    }

    @Test
    public void removeUser() throws AbstractException {
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken);
        removeRequest.setLogin(USUAL_USER_LOGIN);
        Assert.assertNotNull(userEndpoint.removeUser(removeRequest));
        Assert.assertThrows(Exception.class, () -> getUserToken(USUAL_USER_LOGIN, USUAL_USER_PASSWORD));
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USUAL_USER_LOGIN);
        request.setPassword(USUAL_USER_PASSWORD);
        request.setEmail(USUAL_USER_EMAIL);
        userEndpoint.registryUser(request);
    }

    @Test
    public void unlockUser() throws AbstractException {
        @NotNull final UserUnlockRequest request = new UserUnlockRequest(adminToken);
        request.setLogin(USUAL_USER_LOGIN);
        Assert.assertNotNull(userEndpoint.unlockUser(request));
        Assert.assertNotNull(getUserToken(USUAL_USER_LOGIN, USUAL_USER_PASSWORD));
    }

    @Test
    public void updateProfileUser() throws AbstractException {
        @Nullable final String token = getUserToken(USUAL_USER_LOGIN, USUAL_USER_PASSWORD);
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(token);
        request.setFirstName(USUAL_FIRST_NAME);
        request.setLastName(USUAL_LAST_NAME);
        request.setMiddleName(USUAL_MIDDLE_NAME);
        Assert.assertNotNull(userEndpoint.updateUserProfile(request));
        @NotNull final UserProfileRequest viewRequest = new UserProfileRequest(token);
        @Nullable final UserDTO user = authEndpoint.profile(viewRequest).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(USUAL_FIRST_NAME, user.getFirstName());
        Assert.assertEquals(USUAL_LAST_NAME, user.getLastName());
        Assert.assertEquals(USUAL_MIDDLE_NAME, user.getMiddleName());
    }

}
