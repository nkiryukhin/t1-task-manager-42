package ru.t1.nkiryukhin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.nkiryukhin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.nkiryukhin.tm.api.endpoint.ITaskEndpoint;
import ru.t1.nkiryukhin.tm.api.endpoint.IUserEndpoint;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.dto.model.TaskDTO;
import ru.t1.nkiryukhin.tm.dto.request.*;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.marker.IntegrationCategory;
import ru.t1.nkiryukhin.tm.service.PropertyService;

import java.util.List;

import static ru.t1.nkiryukhin.tm.data.ProjectTestData.PROJECT_ONE_DESCRIPTION;
import static ru.t1.nkiryukhin.tm.data.ProjectTestData.PROJECT_ONE_NAME;
import static ru.t1.nkiryukhin.tm.data.TaskTestData.*;
import static ru.t1.nkiryukhin.tm.data.UserTestData.*;

@Category(IntegrationCategory.class)
public final class TaskEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private static final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private static final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @Nullable
    private String projectOneId;

    @Nullable
    private String taskOneId;

    private int taskOneIndex;

    @Nullable
    private String taskTwoId;

    private int taskTwoIndex;

    @BeforeClass
    public static void setUp() throws AbstractException {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(ADMIN_USER_LOGIN);
        loginRequest.setPassword(ADMIN_USER_PASSWORD);
        adminToken = authEndpoint.login(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USUAL_USER_LOGIN);
        request.setPassword(USUAL_USER_PASSWORD);
        request.setEmail(USUAL_USER_EMAIL);
        userEndpoint.registryUser(request);
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin(USUAL_USER_LOGIN);
        userLoginRequest.setPassword(USUAL_USER_PASSWORD);
        userToken = authEndpoint.login(userLoginRequest).getToken();
    }

    @AfterClass
    public static void tearDown() throws AbstractException {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(USUAL_USER_LOGIN);
        userEndpoint.removeUser(request);
    }

    @Before
    public void before() throws AbstractException {
        taskOneId = createTestTask(TASK_ONE_NAME, TASK_ONE_DESCRIPTION);
        taskOneIndex = 0;
        taskTwoId = createTestTask(TASK_TWO_NAME, TASK_TWO_DESCRIPTION);
        taskTwoIndex = 1;
        projectOneId = createTestProject(PROJECT_ONE_NAME, PROJECT_ONE_DESCRIPTION);
    }

    @After
    public void after() throws AbstractException {
        @NotNull final TaskClearRequest taskClearRequest = new TaskClearRequest(userToken);
        Assert.assertNotNull(taskEndpoint.clearTask(taskClearRequest));
        @NotNull final ProjectClearRequest projectClearRequest = new ProjectClearRequest(userToken);
        Assert.assertNotNull(projectEndpoint.clearProject(projectClearRequest));
    }

    @NotNull
    private String createTestTask(final String name, final String description) throws AbstractException {
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(userToken);
        taskCreateRequest.setName(name);
        taskCreateRequest.setDescription(description);
        return taskEndpoint.createTask(taskCreateRequest).getTask().getId();
    }

    @NotNull
    private String createTestProject(final String name, final String description) throws AbstractException {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName(name);
        projectCreateRequest.setDescription(description);
        return projectEndpoint.createProject(projectCreateRequest).getProject().getId();
    }

    @Nullable
    private TaskDTO findTestTaskById(final String id) throws AbstractException {
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(userToken);
        request.setId(id);
        return taskEndpoint.getTaskById(request).getTask();
    }

    @Test
    public void changeStatusByIdTask() throws AbstractException {
        @NotNull final Status status = Status.COMPLETED;
        @NotNull final TaskChangeStatusByIdRequest taskCreateRequestNullToken = new TaskChangeStatusByIdRequest(null);
        taskCreateRequestNullToken.setId(taskOneId);
        taskCreateRequestNullToken.setStatus(status);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusById(taskCreateRequestNullToken));
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(userToken);
        request.setId(taskOneId);
        request.setStatus(status);
        Assert.assertNotNull(taskEndpoint.changeTaskStatusById(request));
        @Nullable final TaskDTO task = findTestTaskById(taskOneId);
        Assert.assertNotNull(task);
        Assert.assertEquals(status, task.getStatus());
    }

    @Test
    public void changeStatusByIndexTask() throws AbstractException {
        @NotNull final Status status = Status.COMPLETED;
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(userToken);
        request.setIndex(taskOneIndex);
        request.setStatus(status);
        Assert.assertNotNull(taskEndpoint.changeTaskStatusByIndex(request));
        @Nullable final TaskDTO task = findTestTaskById(taskOneId);
        Assert.assertNotNull(task);
        Assert.assertEquals(status, task.getStatus());
    }

    @Test
    public void clearTask() throws AbstractException {
        @NotNull final TaskClearRequest request = new TaskClearRequest(userToken);
        Assert.assertNotNull(taskEndpoint.clearTask(request));
        @Nullable TaskDTO task = findTestTaskById(taskOneId);
        Assert.assertNull(task);
        task = findTestTaskById(taskTwoId);
        Assert.assertNull(task);
    }

    @Test
    public void createTask() throws AbstractException {
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(userToken);
        taskCreateRequest.setName(TASK_THREE_NAME);
        taskCreateRequest.setDescription(TASK_THREE_DESCRIPTION);
        @Nullable TaskDTO task = taskEndpoint.createTask(taskCreateRequest).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_THREE_NAME, task.getName());
        Assert.assertEquals(TASK_THREE_DESCRIPTION, task.getDescription());
    }

    @Test
    public void completeByIdTask() throws AbstractException {
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(userToken);
        request.setId(taskOneId);
        Assert.assertNotNull(taskEndpoint.completeTaskById(request));
        @Nullable TaskDTO task = findTestTaskById(taskOneId);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void completeByIndexTask() throws AbstractException {
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(userToken);
        request.setIndex(taskOneIndex);
        Assert.assertNotNull(taskEndpoint.completeTaskByIndex(request));
        @Nullable TaskDTO task = findTestTaskById(taskOneId);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void listTask() throws AbstractException {
        @NotNull final TaskListRequest request = new TaskListRequest(userToken);
        @Nullable final List<TaskDTO> tasks = taskEndpoint.listTask(request).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        for (TaskDTO task : tasks) {
            Assert.assertNotNull(findTestTaskById(task.getId()));
        }
    }

    @Test
    public void removeByIdTask() throws AbstractException {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(userToken);
        request.setId(taskTwoId);
        Assert.assertNotNull(taskEndpoint.removeTaskById(request));
        Assert.assertNull(findTestTaskById(taskTwoId));
    }

    @Test
    public void removeByIndexTask() throws AbstractException {
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(userToken);
        request.setIndex(taskTwoIndex);
        Assert.assertNotNull(taskEndpoint.removeTaskByIndex(request));
        Assert.assertNull(findTestTaskById(taskTwoId));
    }

    @Test
    public void showByIdTask() throws AbstractException {
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(userToken);
        request.setId(taskOneId);
        @Nullable final TaskDTO task = taskEndpoint.getTaskById(request).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(taskOneId, task.getId());
    }

    @Test
    public void showByIndexTask() throws AbstractException {
        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(userToken);
        request.setIndex(taskOneIndex);
        @Nullable final TaskDTO task = taskEndpoint.getTaskByIndex(request).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(taskOneId, task.getId());
    }

    @Test
    public void startByIdTask() throws AbstractException {
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(userToken);
        request.setId(taskOneId);
        Assert.assertNotNull(taskEndpoint.startTaskById(request));
        @Nullable TaskDTO task = findTestTaskById(taskOneId);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void startByIndexTask() throws AbstractException {
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(userToken);
        request.setIndex(taskOneIndex);
        Assert.assertNotNull(taskEndpoint.startTaskByIndex(request));
        @Nullable TaskDTO task = findTestTaskById(taskOneId);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void updateByIdTask() throws AbstractException {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(userToken);
        request.setId(taskOneId);
        request.setName(TASK_THREE_NAME);
        request.setDescription(TASK_THREE_DESCRIPTION);
        Assert.assertNotNull(taskEndpoint.updateTaskById(request));
        @Nullable TaskDTO task = findTestTaskById(taskOneId);
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_THREE_NAME, task.getName());
        Assert.assertEquals(TASK_THREE_DESCRIPTION, task.getDescription());
    }

    @Test
    public void updateByIndexTask() throws AbstractException {
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(userToken);
        request.setIndex(taskOneIndex);
        request.setName(TASK_THREE_NAME);
        request.setDescription(TASK_THREE_DESCRIPTION);
        Assert.assertNotNull(taskEndpoint.updateTaskByIndex(request));
        @Nullable TaskDTO task = findTestTaskById(taskOneId);
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_THREE_NAME, task.getName());
        Assert.assertEquals(TASK_THREE_DESCRIPTION, task.getDescription());
    }

    private void bindTaskProject(final String projectId, final String taskId) throws AbstractException {
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(userToken);
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        taskEndpoint.bindTaskToProject(request);
    }

    @Test
    public void bindToProjectTask() throws AbstractException {
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(userToken);
        request.setProjectId(projectOneId);
        request.setTaskId(taskOneId);
        Assert.assertNotNull(taskEndpoint.bindTaskToProject(request));
        @NotNull final TaskListByProjectIdRequest getByIdRequest = new TaskListByProjectIdRequest(userToken);
        getByIdRequest.setProjectId(projectOneId);
        @Nullable final List<TaskDTO> tasks = taskEndpoint.listTaskByProjectId(getByIdRequest).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        Assert.assertEquals(projectOneId, tasks.get(0).getProjectId());
        Assert.assertEquals(taskOneId, tasks.get(0).getId());
    }

    @Test
    public void showByProjectIdTask() throws AbstractException {
        bindTaskProject(projectOneId, taskOneId);
        bindTaskProject(projectOneId, taskTwoId);
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(userToken);
        request.setProjectId(projectOneId);
        @Nullable final List<TaskDTO> tasks = taskEndpoint.listTaskByProjectId(request).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        for (TaskDTO task : tasks) {
            Assert.assertEquals(projectOneId, task.getProjectId());
        }
    }

    @Test
    public void unbindFromProjectTask() throws AbstractException {
        bindTaskProject(projectOneId, taskOneId);
        bindTaskProject(projectOneId, taskTwoId);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(userToken);
        request.setProjectId(projectOneId);
        request.setTaskId(taskOneId);
        Assert.assertNotNull(taskEndpoint.unbindTaskFromProject(request));
        @NotNull final TaskListByProjectIdRequest listByProjectIdRequest = new TaskListByProjectIdRequest(userToken);
        listByProjectIdRequest.setProjectId(projectOneId);
        @Nullable final List<TaskDTO> tasks = taskEndpoint.listTaskByProjectId(listByProjectIdRequest).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        Assert.assertEquals(projectOneId, tasks.get(0).getProjectId());
        Assert.assertEquals(taskTwoId, tasks.get(0).getId());
    }

}