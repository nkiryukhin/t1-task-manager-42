package ru.t1.nkiryukhin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.model.TaskDTO;
import ru.t1.nkiryukhin.tm.dto.request.TaskGetByIndexRequest;
import ru.t1.nkiryukhin.tm.dto.response.TaskGetByIndexResponse;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Show task by index";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(getToken());
        request.setIndex(index);
        @NotNull final TaskGetByIndexResponse response = getTaskEndpoint().getTaskByIndex(request);
        @Nullable TaskDTO task = response.getTask();
        showTask(task);
    }

}
